How to fold the resulting document
==================================

*We take as example a 64 pages imposed document, with first page being 1, and
last being 64. Page orientation is not displayed in these examples.*

1. Place your imposed sheet of paper on the table, the first page facing the
   table, in the bottom right corner.

  .. tip::

     +----+----+----+----+
     | 6  | 27 | 30 | 3  |
     +----+----+----+----+
     | 11 | 22 | 19 | 14 |
     +----+----+----+----+
     | 10 | 23 | 18 | 15 |
     +----+----+----+----+
     | 7  | 26 | 31 | 2  |
     +----+----+----+----+

     Page 1 is on the opposite side of page 2, in the bottom right corner.

2. Look at the *visible* page number in the bottom right corner, and fold the
   page to bring the next page on top of this one.

   .. tip::

      At the end of step 1, page 2 is in the bottom right corner, so we fold
      the page according to a horizontal line, to bring page 3 on page 2. The
      result is:

      +----+----+----+----+
      | 12 | 21 | 20 | 13 |
      +----+----+----+----+
      | 5  | 28 | 29 | 4  |
      +----+----+----+----+

3. Repeat step 2, until you have nothing to fold.

   .. tip::

      * Page 4 is in the bottom right corner, so we fold the page according to a
        vertical line, to bring page 5 on page 4. The result is:

        +----+----+
        | 24 | 9  |
        +----+----+
        | 25 | 8  |
        +----+----+

      * We fold so that page 9 goes on page 8.

        +----+----+
        | 17 | 16 |
        +----+----+

      * We fold so that page 17 goes on page 16.

        +----+
        | 32 |
        +----+

      * Nothing to fold. Stop!

4. That's it. Now, you can carefully staple your book, and carefuly cut it so
   that you can read it.


